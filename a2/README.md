

# LIS4368 - Advanced Web App

## Chris Ehster

### Assignment 2 Requirements:
    1. Install MySQL database on our local server
    2. Configure JDBC drivers
    3. Create and Compile a Servlet that interacts with a given MySQL database

#### README.md file should include the following items:

* Screenshots of running installations
* Git commands w/short descriptions
* Links to localhost
  

#### Git commands w/short descriptions:

    1. git init - This command creates a git repository.
    2. git status - This command checks the local repo for any changes. It compares the local repo to a snapshot of the remote repo that was pulled/cloned, and displays stages of said changes (staged, committed).
    3. git add - This command 'stages' changes you made to a repository, which adds it to a queue to prepare for committing.
    4. git commit - This command properly commits the changes you've made, but only on the local repository you are working with. To your local machine, you have officially saved your changes.
    5. git push - This command sends the changes you've committed to the remote repository associated with the directory. This officially saves changes for the remote repo.
    6. git pull - This command grabs the working master copy of the remote repository selected and brings in any changes to your local working repo that may have been added since the last pull.
    7. git branch - This command takes an existing repo and creates a copy that can be used for feature development (so as not to disturb the master branch for production ready projects).

#### Assignment Links:

*'hello' Servlet:*
[A1 hello servlet](http://localhost:9999/hello "A1 hello")

*'HelloHome' Servlet:*
[A1 HelloHome servlet](http://localhost:9999/hello/HelloHome.html "A1 hello/HelloHome")

*'sayhello' Servlet:*
[A1 sayhello servlet](http://localhost:9999/hello/sayhello.html "A1 sayhello")

*'querybook' Servlet:*
[A1 querybook servlet](http://localhost:9999/hello/querybook.html "A1 querybook")


#### Assignment Screenshots:

*Screenshot of 'querybook' running*:

!['querybook' servlet running](img/query.png)




