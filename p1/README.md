

# LIS4368 - Advanced Web App

## Chris Ehster

### Project 1 Requirements:
    1. Generate HTML Input fields for the columns stored in our mySQL database.
    2. Use jQuery form validation with regular expressions to validate input on the client-side.
    3. Add HTML5 attributes to our form fields to specify input length ranges.

#### README.md file should include the following items:

* Screenshots of running installations
* Git commands w/short descriptions
* Links to localhost
  

#### Git commands w/short descriptions:

    1. git init - This command creates a git repository.
    2. git status - This command checks the local repo for any changes. It compares the local repo to a snapshot of the remote repo that was pulled/cloned, and displays stages of said changes (staged, committed).
    3. git add - This command 'stages' changes you made to a repository, which adds it to a queue to prepare for committing.
    4. git commit - This command properly commits the changes you've made, but only on the local repository you are working with. To your local machine, you have officially saved your changes.
    5. git push - This command sends the changes you've committed to the remote repository associated with the directory. This officially saves changes for the remote repo.
    6. git pull - This command grabs the working master copy of the remote repository selected and brings in any changes to your local working repo that may have been added since the last pull.
    7. git branch - This command takes an existing repo and creates a copy that can be used for feature development (so as not to disturb the master branch for production ready projects).
#### Assignment Screenshots:

*Screenshot A3 ERD:*
![Landing Page](img/landing.png "Landing page for the website.")
![Client-Side Fail](img/fail.png "Failed client-side validation.")
![Client-Side Success](img/success.png "Successful client-side validation.")







