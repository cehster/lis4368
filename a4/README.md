# LIS4368 - Advanced Web App

## Chris Ehster

### Assignment 4 Requirements:
1. Modify `context.xml` to ensure that our JDBC connection can successfully access the MySQL database configured in [Assignment 3](../a3/README.md).
2. Create a 'pojo' (plain old java object) to create Customer objects for our database.
   - Server-Side Validation in Java 
3. Implement dynamic server routing with Web Annotations to deliver clients to a `thanks.jsp` page after successfully filling out the form.  

#### README.md file should include the following items:

* Screenshots of application
* Git commands w/short descriptions

#### Git commands w/short descriptions:

    1. git init - This command creates a git repository.
    2. git status - This command checks the local repo for any changes. It compares the local repo to a snapshot of the remote repo that was pulled/cloned, and displays stages of said changes (staged, committed).
    3. git add - This command 'stages' changes you made to a repository, which adds it to a queue to prepare for committing.
    4. git commit - This command properly commits the changes you've made, but only on the local repository you are working with. To your local machine, you have officially saved your changes.
    5. git push - This command sends the changes you've committed to the remote repository associated with the directory. This officially saves changes for the remote repo.
    6. git pull - This command grabs the working master copy of the remote repository selected and brings in any changes to your local working repo that may have been added since the last pull.
    7. git branch - This command takes an existing repo and creates a copy that can be used for feature development (so as not to disturb the master branch for production ready projects).
#### Assignment Screenshots:

*Screenshot A4 ERD:*

![Form](img/blank_form.png "Blank Form")

![Submitted Form](img/submit.png "A Submitted Form")
