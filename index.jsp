<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Chris Ehster">
	<link rel="icon" href="favicon.ico">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<title>Chris Ehster - Developer Portfolio</title>

	<%@ include file="/css/include_css.jsp" %>		

<!-- Carousel styles -->
<style type="text/css">
h2
{
	margin: 0;     
	color: #fff;
	padding-top: 90px;
	font-size: 52px;
	font-family: "trebuchet ms", sans-serif;    
}
.item
{
	background: #333;    
	text-align: center;
	height: 300px !important;
}
.carousel
{
  margin: 20px 0px 20px 0px;
}
.bs-example
{
  margin: 20px;
}

</style>
	
</head>
<body>
	
	<%@ include file="/global/nav_global.jsp" %>	
	
	<div class="container">
		 <div class="starter-template">
						<div class="page-header">
						<%@ include file="/global/header.jsp" %>							
						</div>

<!-- Start Bootstrap Carousel  -->
<div class="bs-example">
	<div
      id="myCarousel"
		class="carousel"
		data-interval="6000"
		data-pause="hover"
		data-wrap="true"
		data-keyboard="true"			
		data-ride="carousel">
		
    	<!-- Carousel indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>   
       <!-- Carousel items -->
        <div class="carousel-inner">

				 <div class="active item" style="background: url(img/slide1.jpg) no-repeat left center; background-size: cover;">
					 <div class="container">
						 <div class="carousel-caption">
								<h3 style="font-size:50px;">Welcome to My Portfolio</h3>
							 <p class="lead">I'm a full-stack developer with a passion for building useable web applications and developing data-driven systems for clients and fellow developers.</p>
							 <a class="btn btn-large btn-primary" href="https://www.linkedin.com/in/cehster/" target="_blank">Connect!&ensp;<i class="fa fa-linkedin-square"></i></a>
						 </div>
					 </div>
				 </div>					

         <div class="item" style="background: url(img/slide2.png) no-repeat left center; background-size: cover;">
                <h2>Full-Stack Development</h2>
                <div class="carousel-caption">
                  <h3>Robust web applications built with intuitive UI on top of efficient data models.</h3>
						 <!--  <img src="img/slide2.png" alt="Slide 2">									 -->						
                </div>
            </div>

         <div class="item" style="background: url(img/slide3.jpg) no-repeat left center; background-size: cover;">
                <h2>Data Analytics</h2>
                <div class="carousel-caption">
                  <h3>A focus on using appropriate data to better inform the development of your information system.</h3>
						<!--  <img src="img/slide3.png" class="img-responsive" alt="Slide 3">							 -->								
                </div>
            </div>

        </div>
        <!-- Carousel nav -->
        <a class="carousel-control left" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="carousel-control right" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>
</div>
<!-- End Bootstrap Carousel  -->

<div class="row" style="padding: 20px 0;">
	<div class="col col-md-4 col-sm-12">
		<div><i class="fa fa-file-code-o" style="font-size: 48px;"></i></div>
		<div><h3>Movie Query Application</h3><p>This project focused on developing my asynchronous web development skills as I used AJAX to create a dynamic web application that fetched information from the IMDB API. </p></br><a href="https://bitbucket.org/cehster/lis2360/src/5c1e9e6d1049c14db2c6ce02dba6b7bb5ddde33d/L8/README.md" class="btn btn-primary">Project</a></div>
	</div>
	<div class="col col-md-4 col-sm-12">
		<div><i class="fa fa-table" style="font-size: 48px;"></i></div>
		<div><h3>Data Mart</h3><p>This project demonstrates my ability to develop snowflake schema data marts which provide business with larger stores of information to manage and analyze their databases.</p></br><a href="https://bitbucket.org/cehster/lis3781/src/2998de1035c3d7a642465b26ae1789c0c0352b84/a5/" class="btn btn-primary">Project</a></div>
	</div>
	<div class="col col-md-4 col-sm-12">
		<div><i class="fa fa-area-chart" style="font-size: 48px;"></i></div>
		<div><h3>Titanic Data Modeling</h3><p>This project required me to pracice data analytics skills to clean a CSV data set with Python. I then used data visualization libraries provided by matplotlib to generate graphs for analysis.</p></br><a href="https://bitbucket.org/cehster/lis4369/src/d0f5c017b278d52037facc805f104df294236640/a4/" class="btn btn-primary">Project</a></div>
	</div>
</div>

 	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
</div> <!-- end container -->

 	<%@ include file="/js/include_js.jsp" %>
	
</body>
</html>
