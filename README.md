> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 - Advanced Web Applications

## Chris Ehster

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket Repo
    - Complete Bitbucket Tutorial
      - (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Install MySQL database on our local server
    - Configure JDBC drivers
    - Create and Compile a Servlet that interacts with a given MySQL database

3. [A3 README.md](a3/README.md "My A3 README.md file") 
    - Create an Entity-Relationship Diagram for a fictional pet store company using MySQL Workbench
    - Forward Engineer SQL script
    - Configure AMPPS-hosted local server with mySQL for this new database

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create JDBC connections to A3 database
    - Server-Side Validation with Java
    - Handle HTTP with Servlets and Web Annotations
   
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create a connection pool for a JDBC connection to mySQL.
    - Use prepared statements and result sets to prevent cross-site scripting attacks.
    - Add CRUD functionality to application.
   
4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Generate HTML Input fields for the columns stored in our mySQL database.
    - Use jQuery form validation with regular expressions to validate input on the client-side.
    - Add HTML5 attributes to our form fields to specify input length ranges.

5. [P2 README.md](p2/README.md "My P2 README.md file")
    - Implement server-side validation to CRUD application.
    - Use prepared statements and result sets to prevent cross-site scripting attacks.
    - Build a responsive table with search and sorting features.
