

# LIS4368 - Advanced Web App

## Chris Ehster

### Assignment 3 Requirements:
    1. Create an Entity-Relationship Diagram for a fictional pet store company using MySQL Workbench
    2. Forward Engineer SQL script
    3. Configure AMPPS-hosted local server with mySQL for this new database

#### README.md file should include the following items:

* Screenshots of running installations
* Git commands w/short descriptions
* Links to localhost
  

#### Git commands w/short descriptions:

    1. git init - This command creates a git repository.
    2. git status - This command checks the local repo for any changes. It compares the local repo to a snapshot of the remote repo that was pulled/cloned, and displays stages of said changes (staged, committed).
    3. git add - This command 'stages' changes you made to a repository, which adds it to a queue to prepare for committing.
    4. git commit - This command properly commits the changes you've made, but only on the local repository you are working with. To your local machine, you have officially saved your changes.
    5. git push - This command sends the changes you've committed to the remote repository associated with the directory. This officially saves changes for the remote repo.
    6. git pull - This command grabs the working master copy of the remote repository selected and brings in any changes to your local working repo that may have been added since the last pull.
    7. git branch - This command takes an existing repo and creates a copy that can be used for feature development (so as not to disturb the master branch for production ready projects).
#### Assignment Screenshots:

*Screenshot A3 ERD:*
![A3 ERD](img/erd.png "ERD based upon A3 Requirements")

#### Assignment Links:

*A3 docs: a3.mwb and a3.sql:*
[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")
[A3 SQL File](docs/a3.sql "A3 SQL Script")






